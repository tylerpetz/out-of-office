# Out of Office 1/22/21 - 2/1/21

Hello everyone, I'll be out of office from Friday Jan 22 - Monday Feb 01.

## WHY??

I'm moving into my house, doing a bit of painting in my new place and cleaning of my old place before I leave.

## How do I get ahold of you?

Ideally, you don't. If you really need to, I won't be far from Slack or email, reach out on there and I will get to it when I can. For anything urgent please reach out to Tyler O, Seany, or Dana.

## Do you have any tickets?

Yes!

### Re-login flow issues for okta

[Branch](https://gitlab.com/booj/admin/peak/-/commits/REMAX-re-login-flow)

Code is currently on QA, awaiting approval and then it can likely be moved up to `prod` .

### Saved search block edits

[Ticket](https://booj.atlassian.net/browse/REMAXWEB-5064)

I haven't started work on this ticket but I have a plan for how to address it, I can address on the next sprint or walk someone else through my plan if it needs to be reassigned.

## Alright cya

<img src="https://media.giphy.com/media/k4ta29T68xlfi/giphy.gif">
