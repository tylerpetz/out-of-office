module.exports = {
  title: 'Tyler Petz',
  description: 'Tyler is out of office!',
  dest: 'public',
  base: process.env.NODE_ENV === 'production' ? '/out-of-office/' : '',
}